package com.example.drughelper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent)
    {
        int id = intent.getIntExtra("com.example.drughelper.id", 0);
        String channelId = intent.getStringExtra("com.example.drughelper.drugname");
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.drawable.pill)
                        .setContentTitle("DrugHelper notification")
                        .setContentText("It's time to take a " + intent.getStringExtra(
                                "com.example.drughelper.drugname"));
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(context);
        notificationManager.notify(id, builder.build());
    }
}

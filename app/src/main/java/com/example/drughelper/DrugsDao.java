package com.example.drughelper;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DrugsDao {
    @Query("SELECT * FROM drugs")
    List<Drugs> getAll();

    @Query("SELECT * FROM drugs WHERE id = :id")
    Drugs getById(long id);

    @Query("SELECT id, dname, dtime, dEndTime From drugs")
    List<Drugs> getNamesAndTimes();

    @Insert
    void insert(Drugs drugs);

    @Update
    void update(Drugs drugs);

    @Delete
    void delete(Drugs drugs);
}

package com.example.drughelper;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class drugAddingActivity extends AppCompatActivity {

    private Button mStartButton, mCancelButton;
    private EditText mDrugNameGetter;
    private DatePicker mDatePicker;
    private TimePicker mTimePicker;
    PendingIntent pendingIntent;
    AlarmManager alarmManager;
    
    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daa);

        mStartButton = findViewById(R.id.buttonStart);
        mCancelButton = findViewById(R.id.buttonCancel);
        mDrugNameGetter = findViewById(R.id.editTextDrugName);
        mDatePicker = findViewById(R.id.datePicker);
        mTimePicker = findViewById(R.id.timePicker1);

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        final DrugsDao drugsDao = AppDatabase.getInstance(this).drugsDao();

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Drugs drugs = new Drugs();
                drugs.dname= mDrugNameGetter.getText().toString();

                long curTime = System.currentTimeMillis()/1000;
                long curDate = curTime - curTime % 86400;

                int year = mDatePicker.getYear();
                int month = mDatePicker.getMonth();
                int day = mDatePicker.getDayOfMonth();
                int hours = mTimePicker.getHour();
                int minutes = mTimePicker.getMinute();


                long begTimestamp = curDate + hours * 3600 + minutes * 60;

                if (begTimestamp < curTime) {
                    begTimestamp += 86400;
                }

                Calendar lastday = new GregorianCalendar(year, month, day);
                lastday.add(Calendar.HOUR, hours);
                lastday.add(Calendar.MINUTE, minutes);

                drugs.dtime = begTimestamp;
                drugs.dEndTime = lastday.getTimeInMillis()/1000 + 360;

                //Intent intent = new Intent(
                //        drugAddingActivity.this, AlarmReceiver.class);
                //intent.putExtra("com.example.drughelper.drugname", drugs.dname);
                //pendingIntent = PendingIntent.getBroadcast(drugAddingActivity.this,
                //        0, intent, 0);
//
                //alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                //        (begTimestamp - 10800)*1000,
                //        /*AlarmManager.INTERVAL_DAY*/ 10000, pendingIntent);

                drugsDao.insert(drugs);
                Intent mIntent = new Intent(
                        drugAddingActivity.this, MainActivity.class);
                startActivity(mIntent);
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(
                        drugAddingActivity.this, MainActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
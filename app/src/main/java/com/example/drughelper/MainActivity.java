package com.example.drughelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;


public class MainActivity extends AppCompatActivity {

    private Button mNewButton;
    PendingIntent pendingIntent;
    AlarmManager alarmManager;

    @Override
    public void onCreate (Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNewButton = findViewById(R.id.buttonAddNew);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);


        final DrugsDao drugsDao = AppDatabase.getInstance(this).drugsDao();

        ListView listView = findViewById(R.id.listView);

        List<Drugs> drugs = drugsDao.getNamesAndTimes();

        if (drugs != null) {
            final List<String> drugsList = new ArrayList<>();

            for (int i = 0; i < drugs.size(); i++) {

                if (drugs.get(i).dEndTime*1000 < System.currentTimeMillis()) {
                    Intent intent = new Intent(
                            MainActivity.this, AlarmReceiver.class);
                    pendingIntent = PendingIntent.getBroadcast(MainActivity.this,
                            i, intent, 0);
                    alarmManager.cancel(pendingIntent);
                    drugsDao.delete(drugs.get(i));
                }
                else {
                    String mydName = drugs.get(i).dname;

                    Calendar myCalendar = Calendar.getInstance();
                    myCalendar.setTimeInMillis((drugs.get(i).dtime - 10800) * 1000);
                    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
                    String dateString = formatter.format(myCalendar.getTime());
                    drugsList.add(i, mydName + " - " + dateString);

                    Intent intent = new Intent(
                            MainActivity.this, AlarmReceiver.class);
                    intent.putExtra("com.example.drughelper.drugname", drugs.get(i).dname);
                    intent.putExtra("com.example.drughelper.id", drugs.get(i).id);
                    pendingIntent = PendingIntent.getBroadcast(MainActivity.this,
                            i, intent, 0);

                    alarmManager.cancel(pendingIntent);

                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
                            (drugs.get(i).dtime - 10800) * 1000,
                            /*AlarmManager.INTERVAL_DAY*/ 10000, pendingIntent);
                }

            }

            ArrayAdapter<String> adapter = new ArrayAdapter<>(
                    this, R.layout.list_item, drugsList);
            listView.setAdapter(adapter);
        }

        mNewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(
                        MainActivity.this, drugAddingActivity.class);
                startActivity(mIntent);
            }
        });
    }
}
package com.example.drughelper;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;



@Entity
public class Drugs {
    @PrimaryKey(autoGenerate = true)
    public int id;

    public String dname;

    public long dtime;

    public long dEndTime;
}

